'''
@file Lab_1_Vendotron.py
@brief a file that simulates a vending machine
@details a file that takes in coins and user input to simulate the behavior of 
a vending machine
@author Kevin Lee
@date Jan 12, 2021
'''

import keyboard

class Vendotron:
    '''
    @brief a class that simulates a vending machine
    @details a class that simulates a vending machine by taking in user input 
    from the console
    '''
    
    ##initialization state
    S0_INIT     = 0
    ##waiting state
    S1_WAIT     = 1
    ##dispensing state
    S2_DISPENSE = 2
    ##refund state
    S3_REFUND   = 3
    
    
    
    
    def __init__(self):
        '''
        @biref initializes Vendotron Object
        @details sets up the variables that store prices and the user balance 
        along with needed variables for keyboard input.
        '''
        keyboard.unhook_all ()
        ## state 0-3
        self.state = self.S0_INIT
        ## array representing coins and bills [0] = $0.01 to [7]=$20
        self.payment = [0, 0, 0, 0, 0, 0, 0, 0]
        ## drink prices in cents [c,p,s,d]
        self.prices = [201,101,401,301]
        ## string storing drink can only be "" "c" "p" "s" "d"
        self.drink = ""
        ## global variable for key input
        global pushed_key
        pushed_key = None
        
    def on_keypress(self,thing):
        '''
        @brief function that records key presses
        @details adapted from test_kbd.py from https://bitbucket.org/spluttflob/me405-support/src/master/test_kbd.py
        @param thing input key
        '''
        
        global pushed_key
        pushed_key = thing.name
        
    def run(self):
        '''
        @brief finite state machine that runs the Vendotron indefinitely
        @details finite state machine that operates for the dispensing of one drink at a time
        '''
        keyboard.on_press (self.on_keypress)
        while True:
            #keyboard
            try:
                self.interpret_input(pushed_key)
            except KeyboardInterrupt:
                keyboard.unhook_all ()
                break
            
            #S0
            if self.state == self.S0_INIT:
                self.welcome()
                self.state = self.S1_WAIT
            
            #S1
            elif self.state == self.S1_WAIT:
                if self.drink == "c":
                    if self.get_bal(self.payment)>self.prices[0]:
                        self.state = self.S2_DISPENSE
                    else:
                        print("\nInsufficient Funds | Please insert more coins")
                        self.drink = ""
                elif self.drink == "p":
                    if self.get_bal(self.payment)>self.prices[1]:
                        self.state = self.S2_DISPENSE
                    else:
                        print("\nInsufficient Funds | Please insert more coins")
                        self.drink = ""
                elif self.drink == "s":
                    if self.get_bal(self.payment)>self.prices[2]:
                        self.state = self.S2_DISPENSE
                    else:
                        print("\nInsufficient Funds | Please insert more coins")
                        self.drink = ""
                elif self.drink == "d":
                    if self.get_bal(self.payment)>self.prices[3]:
                        self.state = self.S2_DISPENSE
                    else:
                        print("\nInsufficient Funds | Please insert more coins")
                        self.drink = ""
            
            #S2
            elif self.state == self.S2_DISPENSE:
                if self.drink == "c":
                    self.payment = self.getChange(self.prices[0], self.payment)
                    print("Cuke Dispensed")
                elif self.drink == "p":
                    self.payment = self.getChange(self.prices[1], self.payment)
                    print("Popsi Dispensed")
                elif self.drink == "s":
                    self.payment = self.getChange(self.prices[2], self.payment)
                    print("Spryte Dispensed")
                elif self.drink == "d":
                    self.payment = self.getChange(self.prices[3], self.payment)
                    print("Dr.Pupper Dispensed")
                self.drink = ""
                self.state = self.S3_REFUND
            
            #S3
            elif self.state == self.S3_REFUND:
                change = self.getChange(0,self.payment)
                print("\nDispensing Change")
                if change[0] >0:
                    print("Ejecting " + str(change[0]) + " penny(s).")
                if change[1] >0:
                    print("Ejecting " + str(change[1]) + " nickle(s).")
                if change[2] >0:
                    print("Ejecting " + str(change[2]) + " dime(s).")
                if change[3] >0:
                    print("Ejecting " + str(change[3]) + " quarter(s).")
                if change[4] >0:
                    print("Ejecting " + str(change[4]) + " one(s).")
                if change[5] >0:
                    print("Ejecting " + str(change[5]) + " five(s).")
                if change[6] >0:
                    print("Ejecting " + str(change[6]) + " ten(s).")
                if change[7] >0:
                    print("Ejecting " + str(change[7]) + " twenty(s).")
                self.payment = [0, 0, 0, 0, 0, 0, 0, 0]
                self.state = self.S1_WAIT
                print("\nThank You for using Vendotron\n\n")
                self.welcome()
                    
                    
                    
    
    def welcome(self):
        '''
        @brief displays the welcome message and instructions
        '''
        print("Welcome to Vendotron\n0=penny\n1=nickle\n2=dime\n3=quarter\n4=$1\n5=$5\n6=$10\n7=$20\n\nE=eject\nC=Cuke($2.01)\nP=Popsi($1.01)\nS=Spryte($4.01)\nD=Dr. Pupper($3.01)\n")
    
    def interpret_input(self, key):
        '''
        @brief function that interprets key presses
        @details adapted from test_kbd.py from https://bitbucket.org/spluttflob/me405-support/src/master/test_kbd.py
        '''
        global pushed_key
        if pushed_key:
            bal_change = False
            if pushed_key == "0":
                print ("Penny Inserted")
                self.payment[0] +=1
                bal_change = True

            elif pushed_key == '1':
                print ("Nickle Inserted")
                self.payment[1] +=1
                bal_change = True

            elif pushed_key == '2':
                print ("Dime Inserted")
                self.payment[2] +=1
                bal_change = True
                
            elif pushed_key == '3':
                print ("Quarter Inserted")
                self.payment[3] +=1
                bal_change = True
            
            elif pushed_key == '4':
                print ("Dollar Inserted")
                self.payment[4] +=1
                bal_change = True
            
            elif pushed_key == '5':
                print ("Five Inserted")
                self.payment[5] +=1
                bal_change = True
            
            elif pushed_key == '6':
                print ("Ten Inserted")
                self.payment[6] +=1
                bal_change = True
            
            elif pushed_key == '7':
                print ("Twenty Inserted")
                self.payment[7] +=1
                bal_change = True

            elif pushed_key == 'e':
                print ("Ejecting Coins")
                self.state = self.S3_REFUND
                
            elif pushed_key == 'c':
                print ("Cuke Selected\n")
                self.drink = "c"
                
            elif pushed_key == 'p':
                print ("Popsi Selected\n")
                self.drink = "p"
                
            elif pushed_key == 's':
                print ("Spryte Selected\n")
                self.drink = "s"
                
            elif pushed_key == 'd':
                print ("Dr. Pupper Selected\n")
                self.drink = "d"
                
            if bal_change:
                print("current balance: $"+str(format(self.get_bal(self.payment)/100.0, '.2f')))
                
            pushed_key = None
        

    def get_bal(self, payment):
        '''
        @brief function that calculates the user balance
        @details function that calculates user balance in cents
        @param payment array that represents a collection of coins and bills [0] = $0.01 to [7]=$20
        @return int value of cents in the payment
        '''
        return (1*payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 100*payment[4] + 500*payment[5] + 1000*payment[6] + 2000*payment[7])
        
    def getChange(self, price, payment):
        '''
        @brief function that calculates the change after a purchase/eject
        @details function that calculates change in the least amount of bills/coins
        @param price int value that represents soda prce in cents
        @param payment array that represents a collection of coins and bills
        @return array representing a collection of coins and bills [0] = $0.01 to [7]=$20
        '''
        # determine ballance
        ## int balance of coins an dbills in cents
        self.bal = 1*payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 100*payment[4] + 500*payment[5] + 1000*payment[6] + 2000*payment[7]
        
        #find difference in price
        ## int difference between balance and price
        self.dif = self.bal-price
        ## list of coins and bills to be returned to the user
        self.change = [0, 0, 0, 0, 0, 0, 0, 0]
        
        #check if there are sufficient funds
        if self.dif < 0:
            print("Insufficient Funds")
            return None
        
        #return change
        ## bill and coin denominations from $20 to penny in cents
        self.denoms = [2000,1000,500,100,25,10,5,1]
        ## index of change
        self.idx = 7
        for denom in self.denoms:
            self.change[self.idx] = int(self.dif/denom)
            self.dif -= self.change[self.idx]*denom
            self.idx -=1
        return self.change

#runs the Vendotron object
v = Vendotron()
v.run()   
    
    

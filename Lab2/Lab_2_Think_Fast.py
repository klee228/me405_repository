"""
@file Lab_2_Think_Fast.py
@brief A file that tests reaction time
@details A file that is intended to be run on a Nucleo L476 board to test reaction time
@author Kevin Lee
@date January 26, 2021
"""
import pyb
import utime
import urandom

##initialization state
S0_INIT = 0
##pauses here for 2 to 3 seconds
S1_WAIT = 1
##waits for button press and returns reaction time
S2_LED_ON = 2
##keeps LED on of 1 sec
S3_LED_OFF = 3
##current state
state = S0_INIT

## time of loop start
start_time = 0
## delay in usec
delay = 0
##time of LED activation
activation_time = 0
##time of button press
press_time = 0

## is the button pressed?
button_pressed = False

## LED, adapted from provided lab code
blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

## lsit of reaction times
rxn_list = []

def count_isr (which_pin):        # Create an interrupt service routine
    '''
    @brief interupt function that signals button press
    @details adapted from http://wind.calpoly.edu/ME405/doc/index.html#ss_hardware
    @param which_pin Nucleo pin
    '''
    # print('fn count_isr')
    global button_pressed
    button_pressed = True
    
def start_wait():
    '''
    @brief sets up timings
    @details sets the activation time to be 2 to 3 sec from the start time
    @param which_pin Nucleo pin
    '''
    # print('fn start_wait')
    global activation_time
    start_time = utime.ticks_us()
    delay = urandom.randrange(2000000, 3000000)
    activation_time = utime.ticks_add(start_time, delay)

##external interupt for button press, adapted from http://wind.calpoly.edu/ME405/doc/index.html#ss_hardware  
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine

while True:
    try:
        if state == S0_INIT:
            print("WELCOME TO THINK FAST\nPress the button when the LED turns on\n")
            state = S1_WAIT
            # print("1")
            start_wait()
        elif state == S1_WAIT:
            if utime.ticks_diff(activation_time,utime.ticks_us()) < 0:
                state = S2_LED_ON
                # print("2")
                blinko.high()
                button_pressed = False
        elif state == S2_LED_ON:
            if button_pressed == True:
                button_pressed = False
                press_time = utime.ticks_us()
                rxn_time = utime.ticks_diff(press_time, activation_time)/1000000
                print("reaction time: " + str(rxn_time) + " sec")
                rxn_list.append(rxn_time)
                state = S3_LED_OFF
        elif state == S3_LED_OFF:
                if utime.ticks_diff(utime.ticks_us(),press_time) >1000000:
                    state = S1_WAIT
                    # print("1")
                    start_wait()
                    blinko.low()
    except KeyboardInterrupt:
        if rxn_list == []:
            print("no data collected")
        else:
            rxn_sum = 0
            for rxn in rxn_list:
                rxn_sum += rxn
            print('\nAverage reaction time: ' + str(rxn_sum/len(rxn_list)) + " sec\n")
        break
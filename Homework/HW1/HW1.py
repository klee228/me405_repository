"""
HW1.py
Kevin Lee
1/10/2021
"""

def getChange(price, payment):
    # determine ballance
    bal = 1*payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 100*payment[4] + 500*payment[5] + 1000*payment[6] + 2000*payment[7]
    
    #find difference in price
    dif = bal-price
    change = [0, 0, 0, 0, 0, 0, 0, 0]
    
    #check if there are sufficient funds
    if dif < 0:
        print("Insufficient Funds")
        return None
    
    #return change
    denoms = [2000,1000,500,100,25,10,5,1]
    idx = 7
    for denom in denoms:
        change[idx] = int(dif/denom)
        dif -= change[idx]*denom
        idx -=1
    return tuple(change)
    
#testing
if __name__ == "__main__":
    payment = (0, 1, 2, 3, 4, 5, 6, 7)
    price = 12345678
    print(getChange(price,payment))
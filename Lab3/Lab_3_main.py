'''
@file Lab_3_main.py
@brief A file that runs on the Nucleo L476
@details A file that records voltage values and sends them to the computer
@author Kevin Lee
@date February 2, 2021
'''
import array
import pyb
from pyb import UART

## blue button pin
button = pyb.Pin(pyb.Pin.board.PA0, mode=pyb.Pin.OUT_PP)
##analog to digital
adc = pyb.ADC(button)
##timer 6
tim = pyb.Timer(6, freq = 125000)
##bytearray
buffy = array.array ('H', (0 for index in range (250)))
##initialization state
S0_INIT = 0
##waiting from communication from Spyder state
S1_WAIT = 1
## state recording data when button is pressed
S2_RECORD = 2
##state that sends data to the pc
S3_SEND = 3
#current state
state = S0_INIT
## UART 2
myuart = UART(2)

while True:
    if state == S0_INIT:
        state =S1_WAIT
    elif state == S1_WAIT:
        if myuart.any() != 0:
            val = myuart.readchar()
            state = S2_RECORD
    elif state == S2_RECORD:
        ##temporary ADC value
        temp = adc.read()
        if temp <3000 and temp > 25:
            adc.read_timed(buffy,tim)
            state = S3_SEND
    elif state == S3_SEND:
        myuart.write(str(buffy).encode('ascii'))
        break
"""
@file Lab_3_Spyder.py
@brief A file that runs on Spyder
@details A file that takes user input and returns a image and csv file for volatges of a button after being pressed
@author Kevin Lee
@date February 2, 2021
"""

from matplotlib import pyplot as plt
import csv
import serial
import sys


## Serial port
ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)

class Plotter:
    '''
    @brief class that runs on spyder and plots the voltage response
    @details class that involves a finite state machine that takes user input through the console and creates a csv and png
    '''
    
    def __init__(self):
        '''
        @brief constructor for the Plotter object
        @details initializes the Plotter FSM
        '''
        ##initialization state
        self.S0_INIT = 0
        ## state waiting for G to be pressed
        self.S1_WAIT_G = 1
        ##state waiting for communication from nucleo
        self.S2_WAIT_RESP = 2
        ##state that creates teh csv and png output
        self.S3_PRINT = 3
        
        ##current state
        self.state = self.S0_INIT
        
        ##list of times
        self.times = [t/125000.0*1000000 for t in range(0,250)]
        ##string representing input from nucleo
        self.raw_data = ""
        ##list of data from nucleo cleaned up
        self.data = []
    
    def run(self):
        '''
        @brief Plotter FSM
        @details FSM takes user input and plots the voltage response
        '''
        if self.state == self.S0_INIT:
            self.state = self.S1_WAIT_G
            # print("1")
        elif self.state == self.S1_WAIT_G:
            self.sendChar()
        elif self.state == self.S2_WAIT_RESP:
            self.raw_data = ser.readline().decode('ascii')
            if self.raw_data != "":
                self.state = self.S3_PRINT
                # print("3")
        elif self.state == self.S3_PRINT:
            # print(self.raw_data)
            print("\ndata printed")
            self.process_data()
            self.create_output()
            ser.close
            sys.exit()
          
    def sendChar(self):
        '''
        @brief sends character to nucleo
        @details only sends a character if g or G is entered
        '''
        ##inputted letter
        inv = input('\nPress "G" to start: ')
        if inv == "g" or inv == "G":
            ser.write(str(inv).encode('ascii'))
            self.state = self.S2_WAIT_RESP
            # print("2")
            print("\nPress Blue button\n")
        # myval = ser.readline().decode('ascii')
        # print(myval)
        # ser.close()
        # return myval       
        
    def process_data(self):
        '''
        @brief cleans up string sent by nucleo
        '''
        self.raw_data = self.raw_data[12:-88]
        self.data = list(map(int, self.raw_data.strip(' ').split(", ")))
        # print(self.data)

    def create_output(self):
        '''
        @brief creates csv file and png file of voltage response
        @details files can be found in same directory as the 
        '''
        plt.plot(self.times, self.data)
        plt.title("Voltage vs. Time")
        plt.xlabel("Time (us)")
        plt.ylabel("Voltage (ADC counts)")
        plt.savefig('Lab3.png')
        plt.show()

        with open('Lab3_data.csv', mode='w') as encoder_file:
            ## writes to the csv
            self.value_writer = csv.writer(encoder_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for i in range(len(self.data)):
                self.value_writer.writerow([self.data[i],self.times[i]])
        print('Data exported as Lab3.png and Lab3_data.csv')
        
p = Plotter()
while True:
    p.run()
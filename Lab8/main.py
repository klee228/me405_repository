'''
@file Lab_8_main.py
@brief A file that tests the motor, encoder, and fault detection
@details A program that tests the motor, encoder, and fault detection
@author Kevin Lee and Fernando Estevez
@date March 9, 2021
'''
from Lab_8_MotorDriver import MotorDriver
from Lab_8_EncoderDriver import EncoderDriver
from pyb import Pin, Timer, I2C, ExtInt

#set up encoders drivers
pin1 = pin=Pin.cpu.B6
pin2 = pin=Pin.cpu.B7
pin3 = pin=Pin.cpu.C6
pin4 = pin=Pin.cpu.C7
tim_4 = Timer(4, period=0xFFFF, prescaler=0)
tim_8 = Timer(8, period=0xFFFF, prescaler=0)

##encoder driver 1
ed1 = EncoderDriver(pin1, pin2, tim_4)
##encoder driver 2
ed2 = EncoderDriver(pin3, pin4, tim_8)

#set up motor driver 
IN1 = Pin.cpu.B4
IN2 = Pin.cpu.B5
IN3 = Pin.cpu.B0
IN4 = Pin.cpu.B1
nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=1)
tim_3 = Timer(3, freq=20000)

##motor driver for both motors
md = MotorDriver(nSLEEP, IN1, IN2, IN3, IN4, tim_3)

## i2c communication object
i2c = I2C(1, I2C.MASTER)

## Global Flag that toggles to True when the system detects a fault.
faultDetected = False

## External interupt call back funtion
#
# When call back is triggered, it toggles the fault detected flag to true
#
def callback(Pin):
    '''
    @brief callback method called when nFAULT is triggered
    @details sets fault detected state True and disables the motor
    '''
    global faultDetected
    faultDetected = True
    print('fault detected callback')
    md.disable()
    
def clear_fault(Pin):
    '''
    @brief callback method called when button is pressed
    @details reenables motor if a fault was triggered. Also serves as a way to 
    disable the motor if the fault is not triggered.
    '''
    print('button callback')
    global faultDetected
    if faultDetected:
        #reenable motor
        externInterupt.disable()
        faultDetected = False
        md.enable()
        externInterupt.enable()
    else:
        #emergency shutoff button
        faultDetected = True
        md.disable()

## active-low pin FAULT with a pull-up resistor that isasserted low by the 
# DRV8847 when it detects a fault. 
nFault = Pin(Pin.board.PB2, Pin.IN, Pin.PULL_UP)
## blue button pin on Nucleo L476
button = Pin(Pin.board.PC13, Pin.IN, Pin.PULL_UP)

## Sets up an external interupt when a nFault goes low
externInterupt = ExtInt(nFault, mode=ExtInt.IRQ_FALLING, pull = Pin.PULL_UP,
                        callback=callback)

## Sets up an external interupt when the blue button is pressed
externInterupt_button = ExtInt(button, mode=ExtInt.IRQ_FALLING, 
                               pull = Pin.PULL_UP, callback=clear_fault)

if __name__ == '__main__':
    md.enable()
    
    md.set_duty(1,50)
    # delay(1000)
    # md.set_duty(1,0)
    
    # md.set_duty(2,50)
    # delay(1000)
    # md.set_duty(2,0)
    
    # md.disable()
    
    while True:
        ed1.update()
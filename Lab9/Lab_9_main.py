'''
@file Lab_9_main.py
@brief A file that ballances the ball
@details A file that controls a platform to balance a ball
@author Kevin Lee and Fernando Estevez
@date March 17, 2021
'''

from Lab_9_MotorDriver import MotorDriver
from Lab_9_EncoderDriver import EncoderDriver
from Lab_9_TouchPanel import TouchPanel
from pyb import Pin, Timer, ExtInt
import cotask
import gc
import utime



#||||| ENCODERS |||||

#set up encoders driver pins
Epin1 = Pin.cpu.B6
Epin2 = Pin.cpu.B7
Epin3 = Pin.cpu.C6
Epin4 = Pin.cpu.C7
tim_4 = Timer(4, period=0xFFFF, prescaler=0)
tim_8 = Timer(8, period=0xFFFF, prescaler=0)

##encoder driver 1
ed1 = EncoderDriver(Epin1, Epin2, tim_4)
##encoder driver 2
ed2 = EncoderDriver(Epin3, Epin4, tim_8)

##encoder angle (encoder 1, encoder 2)
enc_ang = (0,0)
##encoder velocity (encoder 1, encoder 2)
enc_vel = (0,0)
##time of last encoder reading
t_last_enc = utime.ticks_ms()
##conversion factor from ticks to radians
TICKS_TO_RAD = 2*3.1416/4000



#||||| MOTORS |||||

#set up motor driver pins
Mpin1 = Pin.cpu.B4
Mpin2 = Pin.cpu.B5
Mpin3 = Pin.cpu.B0
Mpin4 = Pin.cpu.B1
nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=1)
tim_3 = Timer(3, freq=20000)

##motor driver for both motors
md = MotorDriver(nSLEEP, Mpin1, Mpin2, Mpin3, Mpin4, tim_3)
md.enable()

##motor dutys [motor1, motor 2]
mot_duty = [0,0]
##K matrix [v,w,x,theta] for x direction
KX = [-350,-5,-750,-250]
##K matrix [v,w,x,theta] for y direction
KY = [+350,-5,+1000,-250]



#||||| TOUCH PANEL |||||

#set up touch panel pins
Tpin1 = Pin.board.PA7
Tpin2 = Pin.board.PA1
Tpin3 = Pin.board.PA6
Tpin4 = Pin.board.PA0
##Touch panel
tp = TouchPanel(Tpin1, Tpin2, Tpin3, Tpin4,0.176, 0.100, [0,0])

## position of the ball, use scan() (x,y,on panel?)
ball_pos = (0,0,False)
##ball velocity (vx,vy)
ball_vel = (0,0)
##time of last position check
t_last_pos = utime.ticks_ms()




#||||| EXTERNAL INTERUPT |||||

## Global Flag that toggles to True when the system detects a fault.
faultDetected = False

def callback(Pin):
    '''
    @brief callback method called when nFAULT is triggered
    @details sets fault detected state True and disables the motor
    '''
    global faultDetected
    faultDetected = True
    # print('fault detected callback')
    md.disable()
    
def clear_fault(Pin):
    '''
    @brief callback method called when button is pressed
    @details reenables motor if a fault was triggered. Also serves as a way to 
    disable the motor if the fault is not triggered.
    '''
    # print('button callback')
    global faultDetected
    if faultDetected:
        #reenable motor
        externInterupt.disable()
        faultDetected = False
        md.enable()
        externInterupt.enable()
    else:
        #emergency shutoff button
        faultDetected = True
        md.disable()

## nFAult set low when it detects a fault. 
nFault = Pin(Pin.board.PB2, Pin.IN, Pin.PULL_UP)
## blue button pin on Nucleo L476, low when pressed
button = Pin(Pin.board.PC13, Pin.IN, Pin.PULL_UP)

## Sets up an external interupt when a nFault goes low
externInterupt = ExtInt(nFault, mode=ExtInt.IRQ_FALLING, pull = Pin.PULL_UP,
                        callback=callback)

## Sets up an external interupt when the blue button is pressed
externInterupt_button = ExtInt(button, mode=ExtInt.IRQ_FALLING, 
                               pull = Pin.PULL_UP, callback=clear_fault)



#||||| TASKS |||||

def check_panel():
    '''
    @brief task that gets ball position and velocity
    @details updates ball_pos and ball_vel, information used for controlling 
    the motors
    '''
    #update x and v
    global ball_pos
    global ball_vel
    global t_last_pos
    while True:
        new_pos = tp.scan()
        #get velocity
        t_curr_pos = utime.ticks_ms()
        dt = utime.ticks_diff(t_curr_pos, t_last_pos)/1000
        if ball_pos[2]:
            ball_vel = ((new_pos[0]-ball_pos[0])/dt, (new_pos[1]-ball_pos[1])/dt)
        else:
            ball_vel = (0,0)
        t_last_pos = t_curr_pos
        #get position
        ball_pos = new_pos
        # print(str(ball_pos) + "     " + str(mot_duty))
        yield(0)

def check_encoders():
    '''
    @brief task that checks the encoder
    @details updates enc_ang and enc_vel, information is used to update motors
    '''
    #update theta and omega
    global enc_ang
    global enc_vel
    global t_last_enc
    while True:    
        ed1.update()
        ed2.update()
        new_e1 = ed1.get_position()*TICKS_TO_RAD
        new_e2 = ed2.get_position()*TICKS_TO_RAD
        #get angular velocity
        t_curr_enc = utime.ticks_ms()
        dt = utime.ticks_diff(t_curr_enc, t_last_enc)/1000
        enc_vel = ((new_e1-enc_ang[0])/dt, (new_e2-enc_ang[1])/dt )
        t_last_enc = t_curr_enc
        #get position
        enc_ang = (new_e1, new_e2)
        # print(str(enc_vel) + "     " + str(mot_duty))
        yield(0)

def control_motors():
    '''
    @brief task that controls the motors
    @details uses information from the K matrices, panel, and encoders to set 
    motor duty appropriately.
    '''
    #control motors based on given information
    global mot_duty
    while True:  
        calculate_duty()
        md.set_duty(1,mot_duty[0])
        md.set_duty(2,mot_duty[1])
        yield(0)

def calculate_duty():
    '''
    @brief calculates duty for the two motors.
    @details called by caontrol_motors() and uses K matrices, panel, and 
    encoders to update mot_duty
    '''
    #determines motor duty from data readings
    #torque to cuttent to duty cycle
    global ball_pos
    global ball_vel
    global enc_ang
    global mot_duty     
    #if object on panel
    if ball_pos[2]: 
        # print("active")
        #v, w, x, theta
        mot_duty[0] = (KY[0]*ball_vel[1] + KY[1]*enc_vel[0] + KY[2]*ball_pos[1] + KY[3]*enc_ang[0])
        mot_duty[1] = (KX[0]*ball_vel[0] + KX[1]*enc_vel[1] + KX[2]*ball_pos[0] + KX[3]*enc_ang[1])
    else:
        #will not do anything without a ball on it for safety
        mot_duty = [0,0]
    

if __name__ == "__main__":
    ##touch panel task    
    task_panel = cotask.Task (check_panel, name = 'panel', priority = 3, 
                         period = 20, profile = True, trace = False)
    ##encoder task
    task_encoder = cotask.Task (check_encoders, name = 'encoders', priority = 2, 
                         period = 20, profile = True, trace = False)
    ##motor task
    task_motors = cotask.Task (control_motors, name = 'motors', priority = 1, 
                         period = 20, profile = True, trace = False)
    cotask.task_list.append (task_panel)
    cotask.task_list.append (task_encoder)
    cotask.task_list.append (task_motors)

    gc.collect ()

    while True:
        cotask.task_list.pri_sched ()
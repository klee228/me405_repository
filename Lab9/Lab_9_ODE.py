'''
@file Lab_9_ODE.py
@brief A file that solves differential equations related to the platform system
@details A file that represents
@author Kevin Lee
@date February 9, 2021
'''

from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

def my_ode(time, states):
    '''
    @brief ode for platform and ball system
    @param time 
    @param states [x,v,theta,w,tx]
    '''
    ##position m
    x = states[0]
    ##velocity m/s
    v = states[1]
    ##platform angle rad
    theta = states[2]
    ##platform angular velocity rad/s
    w = states[3]
    ##input torque
    tx = states[4]
    
    ##radius of lever arm m
    rm = 0.060 
    ##length of push rod m
    lr = 0.050
    ##radius of ball m
    rb = 0.0105
    ##verical distance from u joint to CG of platform m
    rg = 0.042
    ##horizontal distance from ujoint to push rod pivot m
    lp = 0.110
    ##vertical distance from ujoint to push rod pivot m
    rp = 0.0325
    ##vertical distance from ujoint to platform surface m
    rc = 0.050
    ##mass of the ball kg
    mb = 0.030
    ##mass of the platform kg
    mp = 0.400
    ##moment of interia of the platform thru cg kg/m^2
    ip = 0.00188
    ##moment of intertia of the platform about the ujoint kg/m^2
    ipu = ip + mp*pow(rg,2)
    ##viscous friction at ujoint N*m*s/rad
    b = 0.010
    
    ##gravity m/s^2
    g = 9.81    
    
    ##dx
    dx = v
    ##dv
    dv = -( g*theta)
    ##dtheta
    dtheta = w
    ##dw
    dw = mp*g*rg*theta/ipu - mb*g*x/ipu - mb*g*(rb+rc)*theta/ipu - b*w + ((lp-rp*theta)*tx) / ip / (rm-lr*lp*theta/rm)
    ##dtx
    dtx = 0
    
    return[dx,dv,dtheta,dw,dtx]

def my_ode_closed_loop(time, states):
    '''
    @brief ode for platform and ball system with closed loop control
    @param time 
    @param states [x,v,theta,w,tx]
    '''
    ##position m
    x = states[0]
    ##velocity m/s
    v = states[1]
    ##platform angle rad
    theta = states[2]
    ##platform angular velocity rad/s
    w = states[3]
    ##input torque [ignored]
    tx = states[4]
    
    ##radius of lever arm m
    rm = 0.060 
    ##length of push rod m
    lr = 0.050
    ##radius of ball m
    rb = 0.0105
    ##verical distance from u joint to CG of platform m
    rg = 0.042
    ##horizontal distance from ujoint to push rod pivot m
    lp = 0.110
    ##vertical distance from ujoint to push rod pivot m
    rp = 0.0325
    ##vertical distance from ujoint to platform surface m
    rc = 0.050
    ##mass of the ball kg
    mb = 0.030
    ##mass of the platform kg
    mp = 0.400
    ##moment of interia of the platform thru cg kg/m^2
    ip = 0.00188
    ##moment of intertia of the platform about the ujoint kg/m^2
    ipu = ip + mp*pow(rg,2)
    ##viscous friction at ujoint N*m*s/rad
    b = 0.010
    
    ##gravity m/s^2
    g = 9.81    
    
    ##closed loop resp torque
    tx = 0.05*v - 0.02*w + 0.3*x - 0.2*theta
    
    ##dx
    dx = v
    ##dv
    dv = -( g*theta)
    ##dtheta
    dtheta = w
    ##dw
    dw = mp*g*rg*theta/ipu - mb*g*x/ipu - mb*g*(rb+rc)*theta/ipu - b*w + ((lp-rp*theta)*tx) / ip / (rm-lr*lp*theta/rm)
    ##dtx
    dtx = 0
    
    return[dx,dv,dtheta,dw,dtx]

# results_a = solve_ivp(my_ode, [0,1], [0,0,0,0,0], rtol = 1e-7)

# plt.plot(results_a.t, results_a.y[0])
# plt.title("a) Position vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Position (m)")
# plt.savefig("a_x")
# plt.show()
# plt.close()

# plt.plot(results_a.t, results_a.y[1])
# plt.title("a) Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Velocity (m/sec)")
# plt.savefig("a_v")
# plt.show()
# plt.close()

# plt.plot(results_a.t, results_a.y[2])
# plt.title("a) Platform Angle vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Platform Angle (rad)")
# plt.savefig("a_theta")
# plt.show()
# plt.close()

# plt.plot(results_a.t, results_a.y[3])
# plt.title("a) Angular Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Angular Velocity (rad/sec)")
# plt.savefig("a_w")
# plt.show()
# plt.close()



# results_b = solve_ivp(my_ode, [0,0.4], [0.05,0,0,0,0], rtol = 1e-7)

# plt.plot(results_b.t, results_b.y[0])
# plt.title("b) Position vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Position (m)")
# plt.savefig("b_x")
# plt.show()
# plt.close()

# plt.plot(results_b.t, results_b.y[1])
# plt.title("b) Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Velocity (m/sec)")
# plt.savefig("b_v")
# plt.show()
# plt.close()

# plt.plot(results_b.t, results_b.y[2])
# plt.title("b) Platform Angle vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Platform Angle (rad)")
# plt.savefig("b_theta")
# plt.show()
# plt.close()

# plt.plot(results_b.t, results_b.y[3])
# plt.title("b) Angular Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Angular Velocity (rad/sec)")
# plt.savefig("b_w")
# plt.show()
# plt.close()



# results_c = solve_ivp(my_ode, [0,0.4], [0,0,0.087,0,0], rtol = 1e-7)

# plt.plot(results_c.t, results_c.y[0])
# plt.title("c) Position vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Position (m)")
# plt.savefig("c_x")
# plt.show()
# plt.close()

# plt.plot(results_c.t, results_c.y[1])
# plt.title("c) Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Velocity (m/sec)")
# plt.savefig("c_v")
# plt.show()
# plt.close()

# plt.plot(results_c.t, results_c.y[2])
# plt.title("c) Platform Angle vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Platform Angle (rad)")
# plt.savefig("c_theta")
# plt.show()
# plt.close()

# plt.plot(results_c.t, results_c.y[3])
# plt.title("c) Angular Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Angular Velocity (rad/sec)")
# plt.savefig("c_w")
# plt.show()
# plt.close()



# results_d = solve_ivp(my_ode, [0,0.4], [0,0,0,0,0.001], rtol = 1e-7)

# plt.plot(results_d.t, results_d.y[0])
# plt.title("d) Position vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Position (m)")
# plt.savefig("d_x")
# plt.show()
# plt.close()

# plt.plot(results_d.t, results_d.y[1])
# plt.title("d) Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Velocity (m/sec)")
# plt.savefig("d_v")
# plt.show()
# plt.close()

# plt.plot(results_d.t, results_d.y[2])
# plt.title("d) Platform Angle vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Platform Angle (rad)")
# plt.savefig("d_theta")
# plt.show()
# plt.close()

# plt.plot(results_d.t, results_d.y[3])
# plt.title("d) Angular Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Angular Velocity (rad/sec)")
# plt.savefig("d_w")
# plt.show()
# plt.close()



# results_b_cl = solve_ivp(my_ode_closed_loop, [0,20], [0.05,0,0,0,0], rtol = 1e-7)

# plt.plot(results_b_cl.t, results_b_cl.y[0])
# plt.title("b) Position vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Position (m)")
# plt.savefig("b_x_cl")
# plt.show()
# plt.close()

# plt.plot(results_b_cl.t, results_b_cl.y[1])
# plt.title("b) Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Velocity (m/sec)")
# plt.savefig("b_v_cl")
# plt.show()
# plt.close()

# plt.plot(results_b_cl.t, results_b_cl.y[2])
# plt.title("b) Platform Angle vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Platform Angle (rad)")
# plt.savefig("b_theta_cl")
# plt.show()
# plt.close()

# plt.plot(results_b_cl.t, results_b_cl.y[3])
# plt.title("b) Angular Velocity vs Time")
# plt.xlabel("Time (sec)")
# plt.ylabel("Angular Velocity (rad/sec)")
# plt.savefig("b_w_cl")
# plt.show()
# plt.close()